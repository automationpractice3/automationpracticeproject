*** Settings ***
Library        SeleniumLibrary
Library        String
Library        DateTime
Library        Collections
Resource       /Users/mts_resende/Documents/AutomationPractice/SuiteSiteTestePracticePO_antes_de_refatorar/automationpracticeproject/resources/Resource.robot  

*** Keywords ***


Preencher os dados obrigatórios
    Wait Until Element Is Visible    xpath=//*[@id="account-creation_form"]//h3[contains(text(),"Your personal information")]
    Click Element                    id=id_gender2
    ${first_name} =                  Generate Random Name
    ${last_name} =                   Generate Random Name
    ${password} =                    Generate Random Password
    Input Text                       id=customer_firstname    ${first_name}
    Input Text                       id=customer_lastname     ${last_name}
    Input Text                       id=passwd                ${password}
    Tirar Screenshot                 Preencher os dados obrigatórios
    # Adicione os demais campos e ações necessárias para completar o cadastro


Generate Random Name
    ${random_name} =    Evaluate    ''.join([random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(10)])
    [Return]    ${random_name}   # Retorna um nome aleatório sem números

Generate Random Password
    ${random_password} =    Generate Random String    6    # 6 é o comprimento da senha aleatória
    [Return]    ${random_password}    # Retorna a senha aleatória

Get Current Date With Format
    ${current_date} =    Get Current Date    result_format=%Y%m%d%H%M%S    # Formato da data e hora
    [Return]    ${current_date}    # Retorna a data atual no formato especificado
clear    Input Text                      id=address1              Rua Framework, Bairro Robot
    Input Text                      id=city                  Floripa
    Set Focus To Element            id=id_state
    ### No firefox ocorreu problema ao achar o listbox State, então coloquei um if para esperar
    ### pelo elemento quando for firefox
    Run Keyword If    '${BROWSER}'=='firefox'  Wait Until Element Is Visible   id=id_state
    Select From List By Index       id=id_state              9
    Input Text                      id=postcode              12345
    Input Text                      id=phone_mobile          99988877
    

Submeter cadastro
    Click Button    submitAccount
    Tirar Screenshot                Get Current Date With Format

    #### Conferências
Conferir se o cadastro foi efetuado com sucesso
    Wait Until Element Is Visible    xpath=//*[@id="center_column"]/p
    Element Text Should Be           xpath=//*[@id="center_column"]/p
    ...    Your account has been created.
    Tirar Screenshot                  Conferir se o cadastro foi efetuado com sucesso


