*** Settings ***
Library    SeleniumLibrary
Library    String
Resource    /Users/mts_resende/Documents/AutomationPractice/SuiteSiteTestePracticePO_antes_de_refatorar/automationpracticeproject/resources/Resource.robot

*** Variables ***
${URL}            https://www.automationpractice.pl/index.php

*** Keywords ***
#### Ações
Acessar a página home do site
    Go To              ${URL}  
    Tirar Screenshot     Acessar a página home do site

Digitar o nome do produto "${PRODUTO}" no campo de pesquisa
    Wait Until Element Is Visible    //*[@id="search_query_top"]
    Input Text          //*[@id="search_query_top"]   ${PRODUTO}

    Tirar Screenshot         Digitar o nome do produto "${PRODUTO}" no campo de pesquisa    
Clicar no botão pesquisar
    Click Element       //*[@id="searchbox"]/button

Clicar no botão "Add to Cart" do produto
    Wait Until Element Is Visible   //*[@id="best-sellers_block_right"]/div/ul/li[1]/a/img
    Click Element                   //*[@id="best-sellers_block_right"]/div/ul/li[1]/a/img
    Click Element                   //*[@id="color_8"]
    Wait Until Element Is Visible   xpath=//*[@id="add_to_cart"]/button
    Click Button                    xpath=//*[@id="add_to_cart"]/button
    Tirar Screenshot                Clicar no botão "Add to Cart" do produto 
Clicar no botão "Proceed to checkout"
    Wait Until Element Is Visible   xpath=//*[@id="layer_cart"]//a[@title="Proceed to checkout"]
    Click Element                   xpath=//*[@id="layer_cart"]//a[@title="Proceed to checkout"]

Adicionar o produto "${PRODUTO}" no carrinho
    Digitar o nome do produto "${PRODUTO}" no campo de pesquisa
    Clicar no botão pesquisar
    Clicar no botão "Add to Cart" do produto
    Clicar no botão "Proceed to checkout"
    Tirar Screenshot      Adicionar o produto "${PRODUTO}" no carrinho

