# AutomationPracticeProject

Automação de Testes no Portal Automation Practice

Este projeto apresenta a automação dos testes para remover um produto do carrinho e realizar o cadastro de cliente no portal Automation Practice. Utilizei o padrão de Page Objects para tornar os testes mais robustos e de fácil manutenção.

Descrição do Projeto

O objetivo deste projeto é automatizar os testes essenciais para garantir a funcionalidade correta do sistema no portal Automation Practice. Os testes cobrem duas funcionalidades principais:

Remoção de Produto do Carrinho: Garante que o processo de remoção de produtos do carrinho esteja funcionando corretamente, garantindo uma experiência de usuário sem problemas.
Cadastro de Cliente: Verifica se o processo de cadastro de cliente no portal está funcionando conforme o esperado, garantindo que novos usuários possam se cadastrar com sucesso.
Tecnologias Utilizadas

Linguagem de Programação: Python
Ferramenta de Automação: Robot Framework


By Matheus Henrique Resende

