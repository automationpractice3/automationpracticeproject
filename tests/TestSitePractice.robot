*** Settings ***
Resource            ../resources/Resource.robot
Resource            /Users/mts_resende/Documents/AutomationPractice/SuiteSiteTestePracticePO_antes_de_refatorar/automationpracticeproject/resources/PageObjects/Home.robot
Resource            /Users/mts_resende/Documents/AutomationPractice/SuiteSiteTestePracticePO_antes_de_refatorar/automationpracticeproject/resources/PageObjects/Carrinho.robot
Resource            /Users/mts_resende/Documents/AutomationPractice/SuiteSiteTestePracticePO_antes_de_refatorar/automationpracticeproject/resources/PageObjects/Login.robot
Resource            /Users/mts_resende/Documents/AutomationPractice/SuiteSiteTestePracticePO_antes_de_refatorar/automationpracticeproject/resources/Cadastro.robot

Test Setup          Abrir navegador
#Test Teardown       Fechar navegador

*** Keywords ***


*** Test Case ***
Caso de Teste com PO 01: Remover Produtos do Carrinho
        Acessar a página home do site
        Adicionar o produto "Printed Summer Dress" no carrinho
        Excluir o produto do carrinho
        Conferir se o carrinho fica vazio

### EXERCÍCIO
Caso de Teste com PO 02: Adicionar Cliente
        Acessar a página home do site
        Clicar em "Sign in"
        Informar um e-mail válido
        Clicar em "Create an account"
        Preencher os dados obrigatórios
        Submeter cadastro
        Conferir se o cadastro foi efetuado com sucesso
