*** Settings ***
Library    SeleniumLibrary
Library    String
Library    DateTime
Resource    ../Resource.robot
Library    Collections
*** Keywords ***

Informar um e-mail válido
    Wait Until Element Is Visible   id=email_create
    ${EMAIL}                        Generate Random String
    Input Text                      id=email_create    ${EMAIL}@testerobot.com
    Tirar Screenshot                Informar um e-mail válido

Clicar em "Create an account"
    Click Button         id=SubmitCreate
    Tirar Screenshot     Clicar em "Create an account"