*** Settings ***
Library    SeleniumLibrary
Library    String
Library    DateTime
Library    Collections
Library    OperatingSystem

*** Variables ***
${BROWSER}      chrome

*** Keywords ***
#### Setup e Teardown
Abrir navegador
    Open Browser        about:blank   ${BROWSER}

Fechar navegador
    Close Browser

Clicar em "Sign in"
    Click Element    xpath=//*[@id="header"]//*[@class="login"][contains(text(),"Sign in")]




Tirar Screenshot
    [Arguments]    ${screenshot_name}
    ${screenshot_file} =    Join Path    ${EXECDIR}    ${screenshot_name}.png
    Capture Page Screenshot    ${screenshot_file}
    Log    Capturada screenshot: ${screenshot_file}

  